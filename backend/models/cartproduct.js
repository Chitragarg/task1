var mongoose = require("mongoose");

var Schema = mongoose.Schema;

module.exports = mongoose.model("CartModel",new Schema({
    id:String,
	name:String,
	price:Number,
	quantity:Number
}));