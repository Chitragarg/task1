var mongoose = require("mongoose");

var Schema = mongoose.Schema;

module.exports = mongoose.model("Product", new Schema({
	id:String,
	name:String,
	price:Number,
	stock:Number

}));