describe('controllers',function(){
	beforeEach(module('ProductCatalogApp'));
	var $controller;
	beforeEach(inject(function(_$controller_){
		$controller = _$controller_;

	}));
	describe('OutOfStock',function(){
		it('Checks the stock and alert out of stock',function(){
			var $scope={};
			bvar controller = $controller('MyCartController',{$scope:$scope});

			$scope.newquantity = 5;
			$scope.quantity = 1;
			$scope.stock = 3;
			spyOn(window,'alert');
			OutOfStock();
			expect(window.alert).toHaveBeenCalledWith('Out of Stock');
		});
	});
});
