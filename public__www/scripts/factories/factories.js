var app = angular.module("factories",["ngResource"]);

	app.factory("ProductCatalog",function($resource){
        return $resource("/api/Productlist/:id", {"id":"@id"},{
        	update:{
        		method:'PUT'
        	}
        });
	});
	app.factory("WebShopProductFactory",function($resource){
		return $resource("/api/WebShopProduct/",{

			},{
				query:{
					url:'/api/WebShopProduct/:MinPrice/:MaxPrice/:PageNumber',
					method: 'GET',
					isArray:true,
					params:{
						'MinPrice':'@MinPrice',
						'MaxPrice':'@MaxPrice',
						'PageNumber':'@PageNumber'
					}
				}
			});
	});

    app.factory("CartProductFactory",function($resource){
    	return $resource("/api/CartProduct/:id",{"id":"@id"},{
    		update:{
    			method:'PUT'
    		}
    	});
    });
