var app = angular.module("ProductCatalogApp",["ngRoute","controllers"]);
app.config(['$routeProvider',
	function($routeProvider){
		$routeProvider 
		.when('/addproduct',{
			templateUrl:'views/addproduct.html',
			controller:'AddProductController'
		})
		.when('/productlist',{
			templateUrl:'views/productlist.html',
			controller:'ProductListController'
		})
		.when('/shopingproduct',{
			templateUrl:'views/shopingproduct.html',
			controller:'ShoppingProductController'
		})
		.when('/mycart',{
			templateUrl:'views/mycart.html',
			controller:'MyCartController'
		})
		.otherwise({
			redirectTo:'/shopingproduct'
		});

	}]);

app.directive('navigationBar', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/navigationbar.html'
	};
});
