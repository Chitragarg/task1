var app = angular.module("controllers",["factories","ngTable"]);
var PageNumber = 1;
var items = 10;
var itemsOnPage = 5;
var MinPrice= 1;
var MaxPrice =10000;
var amount = 0;
var template;

//Adding Product
app.controller("AddProductController",function($scope,ProductCatalog)
{
	$scope.addbutton = function()
	{
		console.log("calling add button");
		var newproduct = new ProductCatalog();
		newproduct.id = $scope.id;
		newproduct.name = $scope.name;
		newproduct.price = $scope.price;
		newproduct.stock = $scope.stock;
		newproduct.$save();
		$scope.id = "";
		$scope.name = "";
		$scope.price = "";
		$scope.stock = "";
	}
});//End of Add product Controller

app.controller("ProductListController",function($scope,ProductCatalog)
{
	var list = ProductCatalog.query(function()
	{
		console.log(list);
		$scope.productlist = list;
	});
	
	//Deleting one product
	$scope.DeleteProduct = function(productid)
	{
		console.log("product id ",productid);
		for (var i in list)
		{
			if(list[i].id == productid)
			{
				ProductCatalog.delete({"id":productid},function()
				{
                     console.log("Product deleted");
				});
				list.splice(i,1);
			}
		}
	}//End of delete product

	//functionality for edit butoon in Product catalog 
	$scope.Editform = function(productid)
	{
		console.log("opening editform",productid);
		$scope.show = true;
		ProductCatalog.get({id:productid},function(product)
		{
			console.log(product);
			$scope.productdetail = product;
		});
	}//Editform closes here

	//Updating the product
	$scope.update = function(productid)
	{
		console.log("id",productid);
		ProductCatalog.update({"id":productid},$scope.productdetail,function()
		{
			console.log("updated data");
			$scope.show = false;
		});
   window.location.reload(true);
	}//Update function closes here.
	
});

app.controller('ShoppingProductController',['$scope','ProductCatalog','CartProductFactory','WebShopProductFactory',
	              function($scope,ProductCatalog,CartProductFactory,WebShopProductFactory)
{

//Query to get data on page load
		var list = WebShopProductFactory.query({    'MinPrice':MinPrice,
					                                'MaxPrice':MaxPrice,
					                                'PageNumber':PageNumber },function(){		
												     console.log(list);
												     $scope.productlist = list;
												   });
$scope.orderByMe = function(product){
	$scope.myOrderBy = product;
}
$scope.byRange = function(fieldName,minValue,maxValue){
	 if(minValue === undefined)minValue = Number.MIN_VALUE;
	 if(maxValue === undefined)maxValue = Number.MAX_VALUE;
	return  function predicateFunc(item) {
          return minValue <= item[fieldName] && item[fieldName] <= maxValue;
        };
}
//Displaying pagination
    
	$(function(){
		$("#PaginationPlaceholder").pagination({
			
			items:100,
			itemsOnPage:5,
			cssStyle:'light-theme',
			PageNumber:PageNumber,
			template:template,
			onPageClick: function(page){
				console.log(page);
				PageNumber = page;
				console.log(itemsOnPage);				
				console.log(PageNumber,MinPrice);
				WebShopProductFactory.query({'MinPrice':MinPrice,'MaxPrice':MaxPrice,'PageNumber':PageNumber},function(list)
		        {
		 	     $scope.productlist = list;
		 	     
                });

                //$('#shopingproduct').html(changePage(PageNumber));
						
			}

		});

	});
	// NOTE - 'backend Api is working fine but -Ajax call is not working to load the page due to plugin issue'
	// function changePage(pagenumber){
	// 	pnumber = pagenumber || 1;
	// 	$.ajax({
	// 		type: 'GET',
	// 		dataType: 'json',
	// 		url: '/api/WebShopProduct/MinPrice/MaxPrice/' + pnumber,
	// 		success: function(shopingproduct){
	// 			var template = $('#shopingproduct').html();
	// 			console.log(template);
	// 			var info = Mustache.render(template,shopingproduct);
				
	// 			$('#shopingproduct').html(info);
	// 		}
	// 	});
	// }
	//changePage();
		
  
	//Getting data from database after applying filter for price range.
	$scope.getlist = function()
	{
		 console.log("get list");
		 MaxPrice = $scope.MaxPrice ;
		 MinPrice = $scope.MinPrice;
		 console.log(MaxPrice,MinPrice);
		 console.log('page number',PageNumber);
		 WebShopProductFactory.query({'MinPrice':MinPrice,
		 	                          'MaxPrice':MaxPrice,
			 	                      'PageNumber':PageNumber},function(list)
											 {
											 	   $scope.productlist = list;
									         });									 
    }//End of getlist function
     
    //Adding Product to cart
	$scope.AddToCart = function(product)
	{
		
		console.log("id",product.id);
		console.log("quantity",product.quantity);
		//Checking whether desired Stock is available or not
		if (product.stock >= product.quantity)
		 {
			var ProductInCart = new CartProductFactory();
			ProductInCart.id = product.id;
			ProductInCart.name = product.name;
			ProductInCart.price = product.price;
			ProductInCart.quantity = product.quantity;
			//Stock to updated in Product catalog
			product.stock = product.stock - product.quantity;
			ProductInCart.$save();
			product.quantity="";
			console.log(product);    
			//Updating stock when product added to cart                  
			ProductCatalog.update({"id":product.id},product,function()
			{
			console.log("updated stock",product.stock);
			});		
		}
		else(alert("Available stock is "+ product.stock));	
	}	
}]);

//My Cart Controller	
app.controller("MyCartController",['$scope','ProductCatalog','CartProductFactory',
	              function($scope,ProductCatalog,CartProductFactory)
{   
   //getting list of all products in cart and then calculates total amount to pay
	var list = CartProductFactory.query(function()
		{  
			console.log(list);
			$scope.productlist = list;	
			//calculates total amount to pay
			for(var i=0; i<list.length;i++)
			{
				amount= amount + (list[i].price * list[i].quantity);				
			}  
			$scope.amount_to_pay = amount;	
			    console.log(amount);				
	    });
	
    
    //Deleting product from cart
    $scope.DeleteProduct = function(productincart)
    {
		console.log("product id ",productincart.id);
		for (var i in list)
		{
			if(list[i].id == productincart.id)
			{
				CartProductFactory.delete({"id":productincart.id},function()//Deletes product from cart database
				{
                   console.log("Item deleted from cart databaes");
				});
				list.splice(i,1);//deletes products from HTML Page
			}
		}
			amount = amount - (productincart.price * productincart.quantity);
			$scope.amount_to_pay = amount;//scope for amount_to_pay updated
		
		//Getting data from Product catalog to update stock
		ProductCatalog.get({id:productincart.id},function(product)
		{
            product.stock = product.stock + productincart.quantity; // quantity to be added to stock.
            ProductCatalog.update({"id":product.id},product,function()//Stock updated
            { 
			    console.log("Updated stock in catalog",product.stock);
		 });
		});
	}//end of delete product function

//showing the edit quantity input box
	$scope.Edit = function(productid)
	{
		console.log("opening editform",productid);
		$scope.show = true;
		CartProductFactory.get({id:productid},function(product)
		{
			console.log(product);
			$scope.productdetail = product;
		});
	}
//Closing edit quantity form on cancel button
    $scope.cancel = function(){
    	$scope.show = false;
    }
    //Checks whether stock is available on editing quantity in cart
    function OutOfStock(newquantity,quantity,stock){
         if((newquantity - quantity)> stock){
         	alert('Out of Stock');
         	exit();
         }
    }
	//updating quantity in my cart
	$scope.UpdateQuantity = function(productid)
	{   var updatedquantity = $scope.productdetail.quantity;
		    //getting product to check for availabity of stock and to update it.
			ProductCatalog.get({id:productid},function(productincatalog)
			{           
	           CartProductFactory.get({id:productid},function(product)
		         {
		         	console.log('stock availabe',productincatalog.stock);
		            console.log('previous quantity',product.quantity);
		            console.log('updated quantity',updatedquantity);
		            if(updatedquantity > product.quantity)
		            {
		            	OutOfStock(updatedquantity,product.quantity,productincatalog.stock);
		            	productincatalog.stock = productincatalog.stock - (updatedquantity-product.quantity);
		            	amount = amount+ ((updatedquantity-product.quantity)*product.price);
		            }else{
		            	productincatalog.stock = productincatalog.stock + (product.quantity - updatedquantity);
		            	amount = amount- ((product.quantity-updatedquantity)*product.price);
		            }	 
		            $scope.amount_to_pay = amount;   
		             ProductCatalog.update({"id":productid},productincatalog,function(){
                          console.log("stock updated", productincatalog.stock);
		             }) ;   
												
					CartProductFactory.update({"id":productid},$scope.productdetail,function()
                       {
						console.log("updated data");
						$scope.show = false;
			           });
			           window.location.reload(true);				  
				});
		    });
	}	
}]);//End of My cart controller


	