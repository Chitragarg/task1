var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var ProductModel = require("./backend/models/product.js");
var CartModel = require("./backend/models/cartproduct.js");
var config = require("./config");
//var mongoosepaginate = require('mongoose-pagination');
mongoose.connect(config.database);

app.use(express.static(path.join(__dirname,"public__www")));
app.use(bodyParser.json());

//Adding Product in PRoduct Catalog
app.post("/api/Productlist/:id",function(req,res){
	console.log("posting")
	var product = new ProductModel({
         id:req.body.id,
         name:req.body.name,
         price:req.body.price,
         stock:req.body.stock
	});
	product.save(function(err,product,count){
		if(err){
			console.log("cant save data")
		} 
		   
	});
	return res.send("success");
});


//Getting list of all Products in Product Catalog
app.get("/api/Productlist",function(req,res){

	
	 ProductModel.find()	 	
     .exec(function(err,items){
     	if(err){
     		console.log("error finding data");
     	}
     	else{
     		console.log("Data recieved", items);
     		res.send(JSON.stringify(items));
     	}
     	});
     });

//getting list of all products in web shop
app.get("/api/WebShopProduct/:MinPrice/:MaxPrice/:PageNumber",function(req,res){
	console.log("min price",req.params.MinPrice,req.params.MaxPrice,req.params.PageNumber);
	ProductModel.find().where('price').gt(req.params.MinPrice).lt(req.params.MaxPrice).sort('name')
	.skip(5*(req.params.PageNumber -1)).limit(5).exec(function(err,items){
		if(err){
			console.log("cant find data");

		}else{
			console.log("sending data", items);
			res.send(JSON.stringify(items));
		}
	});
});


//Deleteing a particular product in product catalog
app.delete("/api/Productlist/:id",function(req,res){
	ProductModel.remove({"id":req.params.id},function(err,item){
		if(err){
			console.log("Cant delete data");
		}
	});
	res.send("success");
});

//getting a particular product to be edited in product catalog
app.get("/api/Productlist/:id",function(req,res){
	console.log("id",req.params.id);
	ProductModel.findOne({"id":req.params.id},function(err,product){
		if(err){
			console.log("cant find product");
			var empty = {}
			res.send(JSON.stringify(empty));
		}else{
			res.send(JSON.stringify(product));
			console.log(product);
		}
	});
});

//Updating a product after editing in product catalog
app.put("/api/Productlist/:id",function(req,res){
	console.log("updated data",req.body);
	ProductModel.update({"id":req.params.id},req.body,function(err,product){
		if(err){
			console.log("Error in updating");
		}
		res.send("Successfully updated");
	});
});	


//posting product in cart
app.post("/api/CartProduct/:id",function(req,res){
	console.log("Adding Product in cart in database");
	console.log(req.body);
	var product = new CartModel({
		id:req.body.id,
		name:req.body.name,
		price:req.body.price,
		quantity:req.body.quantity
	});
	product.save(function(err,product){
		if(err){
			console.log("Can't save product in cart");
		}
	});
	res.send("success");
});

//Displaying all products in cart
app.get("/api/CartProduct",function(req,res){
	CartModel.find(function(err,items,count){
		if(err){
			console.log("cannot find data");
		}else{
			res.send(JSON.stringify(items));
		}
	});
});

//Deleting product fron MyCart
app.delete("/api/CartProduct/:id",function(req,res){
	console.log("deleting product from cart");
	CartModel.remove({"id":req.params.id},function(err,item){
		if(err){
			console.log("Cant delete data");
		}
	});

	res.send("success");
});

//Getting a product detail to edit quantity
app.get("/api/CartProduct/:id",function(req,res){
	console.log("id",req.params.id);
	CartModel.findOne({"id":req.params.id},function(err,product){
		if(err){
			console.log("cant find product");
			var empty = {}
			res.send(JSON.stringify(empty));
		}else{
			res.send(JSON.stringify(product));
			console.log(product);
		}
	});
});

//updating quatity in cart model
app.put("/api/CartProduct/:id",function(req,res){
	console.log("updated data in cart model",req.body.quantity);
	CartModel.update({"id":req.params.id},req.body,function(err,product){
		if(err){
			console.log("Error in updating");
		}
		res.send("Successfully updated");
	});
});
app.listen(3000);